package subsup;

public class Creature {
    String name;

    public Creature(String name) {
        this.name = name;
    }

    /**
     * 
     * @return the name of the creature
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Creature{" + "name=" + name + '}';
    }
    public String print(){
        return "Creature{" + "name=" + name + '}';
    }
}
