package subsup;

/**
 *
 * @author alex
 */
public class HB extends Creature{
    String lang;

    public HB(String lang, String name) {
        super(name);
        this.lang = lang;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public String toString() {
        return "HB{ name=" + super.getName() + "lang=" + lang + '}';
    }

   
    
    
    
    
}
