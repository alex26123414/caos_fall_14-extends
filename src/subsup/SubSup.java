
package subsup;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alex
 */
public class SubSup {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Creature> list = new ArrayList<>();
        
        list.add(new HB("dansk", "Human"));
        list.add(new Hund("blue", 4, "Dog"));
        list.add(new A(8, "Spider"));
        
        for (Creature c : list) {
            System.out.println(c.print());
        }
    }
    
}
