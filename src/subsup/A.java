/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package subsup;

/**
 *
 * @author alex
 */
public class A extends Creature{
    int legs;

    public A(int legs, String name) {
        super(name);
        this.legs = legs;
    }

    public int getLegs() {
        return legs;
    }

    public void setLegs(int legs) {
        this.legs = legs;
    }

    @Override
    public String toString() {
        return "A{ name=" + super.getName()+ "legs=" + legs + '}';
    }
    
    
}
