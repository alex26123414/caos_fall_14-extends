/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package subsup;

/**
 *
 * @author alex
 */
public class Hund extends A{
    String color;

    public Hund(String color, int legs, String name) {
        super(legs, name);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Hund{ name="+ super.getName() + " legs="+ super.getLegs() + "color=" + color + '}';
    }
    
    
}
